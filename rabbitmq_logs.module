<?php

/**
 * @file
 * Sends logs to rabbit MQ to be written on background.
 */

/**
 * Implements hook_module_implements_alter().
 */
function rabbitmq_logs_module_implements_alter(&$implementations, $hook) {
  // Unset dblog so logs are not written directly to db.
  if ($hook == 'watchdog') {
    unset($implementations['dblog']);
  }
}

/**
 * Implements hook_watchdog().
 */
function rabbitmq_logs_watchdog(array $log_entry) {
  try {
    $broker = message_broker_get();
    // Send log_entry to queue.
    $broker->sendMessage(
      json_encode($log_entry),
      variable_get('rabbitmq_logs_queue', 'logging'),
      array(
        'routing_key' => variable_get('rabbitmq_logs_routing_key', 'log_key'),
        'delivery_mode' => variable_get('rabbitmq_logs_delivery_mode', 2),
      ));
  }
  catch (MessageBrokerException $e) {
    // Log the message directly into database if no message broker
    // available or other exection.
    dblog_watchdog($log_entry);
  }
}

/**
 * Implements hook_message_broker_consumers().
 */
function rabbitmq_logs_message_broker_consumers($self_name) {
  $consumers = array();

  $consumers['getlogs'] = array(
    'queue' => 'logs',
    'callback' => '_rabbitmq_logs_handle_logging',
  );

  return $consumers;
}

/**
 * Callback for the getlogs message consumer.
 */
function _rabbitmq_logs_handle_logging($message, Closure $ack) {

  $log_entry = json_decode($message->body, TRUE);
  dblog_watchdog($log_entry);

  if (!$log_entry['variables']) {
    $log_entry['variables'] = array();
  }

  if (variable_get('rabbitmq_logs_stdout_message', TRUE)) {
    // Print the raw error message the the standard output for debugging.
    echo "LOG: ",
         strtr($log_entry['message'], $log_entry['variables']),
         "\n";
  }

  // Acknowledge the message as processed.
  $ack();
}
